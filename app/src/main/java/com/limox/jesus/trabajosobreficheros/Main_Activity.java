package com.limox.jesus.trabajosobreficheros;

import android.app.ProgressDialog;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;

import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.CountDownTimer;
import android.os.Environment;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;
import com.squareup.picasso.Picasso;

import cz.msebera.android.httpclient.Header;

public class Main_Activity extends AppCompatActivity {

    Button btnDescargar, btnComenzar, btnParar;
    EditText edtUrlFrases, edtUrlImagenes;
    ImageView iVImagenes;
    TextView txvFrases;
    int posArrayImagenes;
    int posArrayFrases;
    int imagenError;
    String[] imagenes;
    String[] frases;
    CountDownTimer timer;
    final static String WEB = "https://treelemonx.000webhostapp.com/upload.php";
    boolean seguir;
    View vista;
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
        btnDescargar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isNetworkAvailable()) {
                    if (!edtUrlImagenes.getText().toString().isEmpty()) {
                        conectarAsincrono(edtUrlImagenes.getText().toString(), "imagenes");
                    } else {
                        Toast.makeText(Main_Activity.this, "Introduce la ruta hacia el fichero", Toast.LENGTH_LONG).show();
                    }
                    if (!edtUrlFrases.getText().toString().isEmpty()) {
                        conectarAsincrono(edtUrlFrases.getText().toString(), "frases");
                    } else {
                        Toast.makeText(Main_Activity.this, "Introduce la ruta hacia el fichero", Toast.LENGTH_LONG).show();
                    }

                } else {
                    Toast.makeText(Main_Activity.this, "No hay conexion a internet", Toast.LENGTH_LONG).show();
                }
            }
        });

        btnComenzar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (imagenes != null && frases != null) {
                    if (imagenes.length > 0 && frases.length > 0) {
                        crearContador();
                        seguir = true;
                        Toast.makeText(Main_Activity.this, "Comenzando", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Snackbar.make(vista, "Error: no hay elementos disponibles", Snackbar.LENGTH_SHORT).show();
                    subirError(simpleDateFormat.format(new GregorianCalendar().getTime())+"Error: no hay elementos disponibles");
                }

            }
        });
        btnParar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (seguir) {
                    seguir = false;
                    Toast.makeText(Main_Activity.this, "Finalizando", Toast.LENGTH_SHORT).show();
                } else {
                    Snackbar.make(vista, "El ciclo no est� corriendo", Snackbar.LENGTH_SHORT).show();
                    subirError(simpleDateFormat.format(new GregorianCalendar().getTime())+"El ciclo no est� corriendo");
                }
            }
        });
    }

    private void init() {

        btnDescargar = (Button) findViewById(R.id.btnDescargar);
        btnComenzar = (Button) findViewById(R.id.btnComenzar);
        btnParar = (Button) findViewById(R.id.btnParar);

        edtUrlImagenes = (EditText) findViewById(R.id.edtEnlaceImg);
        edtUrlFrases = (EditText) findViewById(R.id.edtEnlaceFrs);

        iVImagenes = (ImageView) findViewById(R.id.ivwFotos);

        txvFrases = (TextView) findViewById(R.id.txvFrases);

        posArrayFrases = 0;
        posArrayImagenes = 0;

        imagenError = R.drawable.error;

        vista = findViewById(R.id.activity_main);

        seguir = true;

    }

    private void cargarImagen() {
        if (imagenes.length > 0) {
            Picasso.with(Main_Activity.this).
                    load(imagenes[0]).
                    error(imagenError).
                    into(iVImagenes);
        }
    }

    private void cargarFrases() {
        if (frases.length > 0) {
            txvFrases.setText(frases[0]);
        }
    }

    private void cargarImagenSiguiente() {
        if (imagenes != null)
            if (imagenes.length > 0) {
                posArrayImagenes++;
                if (posArrayImagenes > imagenes.length - 1)
                    posArrayImagenes = 0;
                try {
                    Picasso.with(Main_Activity.this).load(imagenes[posArrayImagenes]).error(imagenError).into(iVImagenes);
                } catch (Exception e) {
                    Snackbar.make(vista, "Error al cargar la imagen " + posArrayImagenes + " " + e.getMessage(), Snackbar.LENGTH_SHORT).show();
                    subirError(simpleDateFormat.format(new GregorianCalendar().getTime())+" Error: "+e.getMessage());
                }
            }
    }

    private void cargarFraseSiguiente() {
        if (frases != null)
            if (frases.length > 0) {
                posArrayFrases++;
                if (posArrayFrases > frases.length - 1)
                    posArrayFrases = 0;
                try {
                    txvFrases.setText(frases[posArrayFrases]);
                } catch (Exception e) {
                    Snackbar.make(vista, "Error al cargar la frase " + posArrayFrases + " " + e.getMessage(), Snackbar.LENGTH_SHORT).show();
                    subirError(simpleDateFormat.format(new GregorianCalendar().getTime())+" Error: "+e.getMessage());
                }
            }
    }

    private boolean isNetworkAvailable() {
        boolean result = false;
        ConnectivityManager cm = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected())
            result = true;

        return result;

    }

    private void conectarAsincrono(String url, String queDescarga) {

        AsyncHttpClient client = new AsyncHttpClient();
        final ProgressDialog progress = new ProgressDialog(Main_Activity.this);
        final String loQueDescarga = queDescarga;
        client.get(url, new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {

                //En caso de error se detiene la barra de progreso y se informa al usuario
                progress.dismiss();
                Toast.makeText(Main_Activity.this, "Error " + throwable.getMessage(), Toast.LENGTH_LONG).show();
                subirError(simpleDateFormat.format(new GregorianCalendar().getTime())+"Error: "+throwable.getMessage());

            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {

                progress.dismiss();
                if (loQueDescarga == "imagenes") {
                    imagenes = responseString.split("\n");
                    Toast.makeText(Main_Activity.this, "Se han guardado " + imagenes.length + loQueDescarga, Toast.LENGTH_SHORT).show();
                    cargarImagen();

                } else {
                    frases = responseString.split("\n");
                    Toast.makeText(Main_Activity.this, "Se han guardado " + frases.length + loQueDescarga, Toast.LENGTH_SHORT).show();
                    cargarFrases();
                }
            }

            @Override
            public void onStart() {
                // called before request is started
                progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progress.setMessage("Conectando . . .");
                progress.setCancelable(false);
                progress.show();
            }
        });
    }

    private void crearContador() {
        timer = new CountDownTimer(5000, 1000) {
            @Override
            public void onTick(long l) {

            }

            @Override
            public void onFinish() {
                cargarFraseSiguiente();
                cargarImagenSiguiente();
                if (seguir) {
                    crearContador();
                }
            }
        }.start();
    }
    private boolean subirError(String error){
        final ProgressDialog progress = new ProgressDialog(this);
        RequestParams params = new RequestParams();
        params.put("contenido",error);
        RestClient.post(WEB, params, new TextHttpResponseHandler() {
            @Override
            public void onStart() {
                super.onStart();
                // called before request is started
                progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progress.setMessage("Subiendo error . . .");
                //progreso.setCancelable(false);
                progress.setOnCancelListener(new DialogInterface.OnCancelListener() {
                    public void onCancel(DialogInterface dialog) {
                        RestClient.cancelRequests(getApplicationContext(), true);
                    }
                });
                progress.show();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Toast.makeText(Main_Activity.this,"Fallo al subir el fallo",Toast.LENGTH_SHORT).show();
                progress.dismiss();}

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                Toast.makeText(Main_Activity.this,"Fallo subido",Toast.LENGTH_SHORT).show();
                progress.dismiss(); }
        });
        return true;
    }

}

