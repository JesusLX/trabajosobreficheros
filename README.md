Ejercicios sobre ficheros
===================

Esta aplicación consta de una *Activity*, compuesta por dos campos de texto editable en los que se introducen la dirección en la red de dos archivos, en la primera hay que poner un archivo que contiene direcciones a otras imagenes y en el segundo una archivo que contenga frases (separadas por saltos de linea). 
Bajo éstas hay un botón para descargar ambos archivos.
Después donde se mostraran las descargas:
Un ImagenView y un TextView donde se verán respectivos archivos.
Debajo hay un par de botones; Cancelar para parar la reproducción de frases e imágenes y Empezar para comenzar a reproducir imágenes y frases con un margen de 5 segundos.

![Screenshot_1480510123.png](https://bitbucket.org/repo/r4bqAy/images/1686418981-Screenshot_1480510123.png) ![Screenshot_1480510128.png](https://bitbucket.org/repo/r4bqAy/images/500337387-Screenshot_1480510128.png)


Sise pulsa el botón Empezar sin que se hayan descargado los paquetes saltará un aviso para el usuario.

![Screenshot_1480510130.png](https://bitbucket.org/repo/r4bqAy/images/2567951143-Screenshot_1480510130.png)

Si hay algún error en la descarga de los archivos saltara otro mensaje y enviará el error a una página web 

![Screenshot_1480510168.png](https://bitbucket.org/repo/r4bqAy/images/3274246744-Screenshot_1480510168.png)


Página de errores: https://treelemonx.000webhostapp.com/errores.txt